const express = require('express');
const cors = require('cors');
const path = require('path');
const app = express();
const PORT = process.env.PORT || 3000;

app.use(cors({credentials: true, origin: true}));
app.use(express.static(path.join(__dirname, 'public')));

// Start the server
app.listen(PORT, () => console.log(`Server running on http://localhost:${PORT}`));
