from os import getenv
from dotenv import load_dotenv

load_dotenv()

def _getenv(var_name:str) -> str:
    var = getenv(var_name)
    if var is None:
        raise Exception('Config var missing')
    return var

ACCESS_TOKEN_EXPIRE_MINUTES = 3600
HASHING_ALGORITHM = "HS256"
SECRET_KEY = getenv('SECRET_KEY')

DB_NAME=_getenv('DB_NAME')
DB_USER=_getenv('DB_USER')
DB_PASSWORD=_getenv('DB_PASSWORD')
DB_URL=_getenv('DB_URL')


