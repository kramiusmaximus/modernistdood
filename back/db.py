# user_db: dict[str,dict] = {
#     'user1@email.com':{
#         'email':'email1@email.com',
#         'hashed_password':12345
#     }
# }

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from config import DB_NAME, DB_USER, DB_PASSWORD, DB_URL

SQLALCHEMY_DATABASE_URL = f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_URL}/{DB_NAME}"

engine = create_engine(SQLALCHEMY_DATABASE_URL)

Base = declarative_base()
Base.metadata.bind = engine

SessionLocal = sessionmaker(autocommit=False, autoflush=True, bind=engine)
