// Function to switch between login and registration forms
import config from './config.js'

document.addEventListener('DOMContentLoaded', (event) => {
    document.getElementById('login-button').addEventListener('click', login);
    document.getElementById('register-button').addEventListener('click', showRegister);
    document.getElementById('register-new-user-button').addEventListener('click', register);
    document.getElementById('register-back-button').addEventListener('click', showLogin);
  });

function showRegister() {
    document.getElementById('login-form').style.display = 'none';
    document.getElementById('register-form').style.display = 'block';
}

function showLogin() {
    document.getElementById('register-form').style.display = 'none';
    document.getElementById('login-form').style.display = 'block';
}

// Login function
function login() {
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    fetch(config.BACKEND_IP + '/login', { // Replace 'your-backend-ip' with your backend IP address
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
    })
    .then(response => response.json())
    .then(data => {
        if (data.token) {
            localStorage.setItem('jwt', data.token); // Save the JWT to localStorage
            window.location.href = 'chat.html'; // Redirect to chat page
        } else {
            document.getElementById('login-error').innerText = data.message;
        }
    })
    .catch(error => console.error('Error:', error));
}

// Register function
function register() {
    const email = document.getElementById('reg-email').value;
    const password = document.getElementById('reg-password').value;
    const confirmPassword = document.getElementById('reg-confirm-password').value;
    if (password !== confirmPassword) {
        document.getElementById('register-error').innerText = 'Passwords do not match.';
        return;
    }
    fetch(config.BACKEND_IP + '/register', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
    })
    .then(response => response.json())
    .then(data => {
        if (data.success) {
            showLogin();
        } else {
            document.getElementById('register-error').innerText = data.message;
        }
    })
    .catch(error => console.error('Error:', error));
}
