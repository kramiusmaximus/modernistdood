from sqlalchemy.orm import Session

import sql_models, pydantic_models

from authentication import hash_password


def get_user(db: Session, user_id: int):
    return db.query(sql_models.User).filter(sql_models.User.id == user_id).first()


def get_user_by_email(db: Session, email: str):
    return db.query(sql_models.User).filter(sql_models.User.email == email).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(sql_models.User).offset(skip).limit(limit).all()


def create_user(db: Session, user: pydantic_models.UserCreateHashed):
    db_user = sql_models.User(email=user.email, hashed_password=user.hashed_password)
    db.add(db_user)
    return db_user

