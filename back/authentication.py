from datetime import timedelta, timezone, datetime
from fastapi import HTTPException, status, Depends
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from passlib.context import CryptContext
from jose import JWTError, jwt
from config import SECRET_KEY, HASHING_ALGORITHM, ACCESS_TOKEN_EXPIRE_MINUTES
from pydantic_models import TokenData, UserCreate
from db import SessionLocal


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
security = HTTPBearer()


def verify_password(password: str, hashed_password: str) -> bool:
    return pwd_context.verify(password, hashed_password)

def hash_password(password: str) -> str:
    return pwd_context.hash(password)

def generate_jwt(email:str):
    to_encode = {
            'sub': email,
            'exp': datetime.now(timezone.utc) + timedelta(ACCESS_TOKEN_EXPIRE_MINUTES)
        }
    return jwt.encode(to_encode, SECRET_KEY, HASHING_ALGORITHM)

# async def get_current_user(credentials: HTTPAuthorizationCredentials= Depends(security)):
#     credentials_exception = HTTPException(
#         status_code=status.HTTP_401_UNAUTHORIZED,
#         detail="Could not validate credentials",
#         headers={"WWW-Authenticate": "Bearer"},
#     )
#     token = credentials.credentials
#     try:
#         payload = jwt.decode(token, SECRET_KEY, algorithms=[HASHING_ALGORITHM])
#         token_data = TokenData(**payload)
#         if token_data.sub is None or token_data.exp <= datetime.now(timezone.utc):
#             raise credentials_exception
    
#     except JWTError:
#         raise credentials_exception
#     email = token_data.sub
#     user = user_db.get(email, None)
#     if user is None:
#         raise credentials_exception
#     return UserAuthModel(**user)
