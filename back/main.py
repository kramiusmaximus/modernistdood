from typing import Annotated
from fastapi import Depends, FastAPI, HTTPException, status
from pydantic_models import UserCreate, UserCreateHashed, UserLogin
from db import SessionLocal
from db_interactions import get_user_by_email, create_user
from authentication import verify_password, generate_jwt, hash_password
import logging

logging.basicConfig(format='%(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
logger = logging.getLogger(__name__)

app = FastAPI()


@app.post("/register/")
async def register_user(user_data:UserCreate):
    with SessionLocal.begin() as session:

        existing_user = get_user_by_email(session, user_data.email)

        if existing_user:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="User with thise email already exists")
    
        new_user = UserCreateHashed(**user_data.model_dump(), hashed_password=hash_password(user_data.password))
        create_user(session, new_user)

    return user_data



@app.post("/login/")
async def login(user_data:UserLogin):
    with SessionLocal.begin() as session:
        user = get_user_by_email(session, user_data.email)
        
        if not user:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Incorrect username or password")
        
        if not verify_password(user_data.password, user.hashed_password):
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Incorrect username or password")
        
        jwt = generate_jwt(user.email)
    
    return {"access_token": jwt, "token_type": "bearer"}


# @app.get("/users/me/", response_model=UserAuthModel)
# async def read_users_me(
#     current_user: Annotated[UserAuthModel, Depends(get_current_user)]
# ):
#     return current_user
