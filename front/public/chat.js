import config from './config.js'

document.addEventListener('DOMContentLoaded', function() {
    const jwt = localStorage.getItem('jwt');
    if (!jwt) {
        window.location.href = 'index.html'; // Redirect to login page if JWT is not found
    } else {
    loadChatLog(jwt); // Load chat history if JWT exists
    }
    });
    
    function loadChatLog(jwt) {
    fetch(config.BACKEND_IP + '/chat/history', { // Replace 'your-backend-ip' with your backend IP address
    method: 'GET',
    headers: {
    'Authorization': `Bearer ${jwt}`
    },
    })
    .then(response => response.json())
    .then(data => {
    if (data.messages) {
    const chatLog = document.getElementById('chat-log');
    chatLog.innerHTML = ''; // Clear existing messages
    data.messages.forEach(msg => {
    const messageElement = document.createElement('div');
    messageElement.innerText = msg;
    chatLog.appendChild(messageElement);
    });
    }
    })
    .catch(error => console.error('Error loading chat log:', error));
    };
    
    function sendMessage() {
    const messageInput = document.getElementById('message-input');
    const message = messageInput.value;
    const jwt = localStorage.getItem('jwt');
    fetch(config.BACKEND_IP + '/send-message', { // Replace 'your-backend-ip' with your backend IP address
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${jwt}`
    },
    body: JSON.stringify({ message }),
})
.then(response => response.json())
.then(data => {
    if (data.message) {
        const chatLog = document.getElementById('chat-log');
        const messageElement = document.createElement('div');
        messageElement.innerText = data.message;
        chatLog.appendChild(messageElement);
        messageInput.value = ''; // Clear input after sending
    }
})
.catch(error => console.error('Error sending message:', error));
}

function logout() {
localStorage.removeItem('jwt'); // Remove JWT from localStorage
window.location.href = 'index.html'; // Redirect to login page
}
