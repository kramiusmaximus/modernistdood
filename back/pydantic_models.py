from pydantic import BaseModel, validator
from datetime import datetime

class UserBase(BaseModel):
    email: str
    full_name: str | None = None

    @validator('email', pre=True, always=True)
    def set_email_to_lowercase(cls, v):
        return v.lower()

class UserCreate(UserBase):
    password: str

class UserLogin(UserBase):
    password: str

class UserCreateHashed(UserBase):
    hashed_password: str

class User(UserBase):
    id: int

    class Config:
        from_attributes = True

class Token(BaseModel):
    access_token: str
    token_type: str

class TokenData(BaseModel):
    sub: str
    exp: datetime
